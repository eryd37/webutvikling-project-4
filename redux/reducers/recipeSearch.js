import { SET_RECIPE_SEARCH_VALUE } from "../actionTypes";

const initialState = {
	searchValue: "",
};

const recipeSearch = (state = initialState, action) => {
	switch (action.type) {
		case SET_RECIPE_SEARCH_VALUE: {
			return { searchValue: action.payload.searchValue };
		}
		default: {
			return state;
		}
	}
};
export default recipeSearch;
