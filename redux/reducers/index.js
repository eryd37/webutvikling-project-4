import { combineReducers } from "redux";
import recipeLoadMode from "./recipeLoadMode";
import wordFrequency from "./wordFrequency";
import recipeFilters from "./recipeFilters";
import recipeSearch from "./recipeSearch";
import recipeLimit from "./recipeLimit";
import recipeSort from "./recipeSort";
import recipePage from "./recipePage";
import favorites from './favorites';
import loading from "./loading";
import history from "./history";

export default combineReducers({
	recipeLoadMode,
	wordFrequency,
	recipeFilters,
	recipeSearch,
	recipeLimit,
	recipeSort,
	recipePage,
	favorites,
	loading,
	history,
});
