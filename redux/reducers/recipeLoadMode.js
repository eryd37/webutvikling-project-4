import { SET_LOAD_MODE_LOAD, SET_LOAD_MODE_APPEND } from "../actionTypes";

const initialState = {
	loadMode: "load"
};

const loadMode = (state = initialState, action) => {
	switch (action.type) {
		case SET_LOAD_MODE_LOAD: {
			return action.payload;
		}
		case SET_LOAD_MODE_APPEND: {
			return action.payload;
		}
		default: {
			return state;
		}
	}
};
export default loadMode;
