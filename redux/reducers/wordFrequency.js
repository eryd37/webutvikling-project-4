import {
	BEGIN_LOADING_WORD_FREQUENCY,
	FAILURE_LOADING_WORD_FREQUENCY,
	SUCCESS_LOADING_WORD_FREQUENCY,
} from "../actionTypes";

const initialState = {
	loading: false,
	wordFrequency: false,
	failure: false,
	error: false,
};

const wordFrequency = (state = initialState, action) => {
	switch (action.type) {
		case BEGIN_LOADING_WORD_FREQUENCY: {
			return { ...state, loading: true, failure: false, error: false };
		}
		case FAILURE_LOADING_WORD_FREQUENCY: {
			return {
				...state,
				loading: false,
				failure: true,
				error: action.payload.error,
			};
		}
		case SUCCESS_LOADING_WORD_FREQUENCY: {
			return {
				...state,
				loading: false,
				failure: false,
				error: false,
				wordFrequency: action.payload.wordFrequency,
			};
		}
		default: {
			return state;
		}
	}
};
export default wordFrequency;
