import { SET_LOADING } from "../actionTypes";

const initialState = {
	loading: false
};

const loading = (state = initialState, action) => {
	switch (action.type) {
		case SET_LOADING: {
			return action.payload;
		}
		default: {
			return state;
		}
	}
};
export default loading;
