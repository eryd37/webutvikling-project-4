import { SET_HISTORY } from "../actionTypes";

const initialState = {
	history: false,
};

const history = (state = initialState, action) => {
	switch (action.type) {
		case SET_HISTORY: {
			return action.payload;
		}
		default: {
			return state;
		}
	}
};

export default history;
