import React, { useState } from "react";
import {
	Body,
	Left,
	Radio,
	ListItem,
	Button,
	Text,
	CheckBox
} from "native-base";
import { View } from "react-native";
import Dialog, {
	DialogContent,
	DialogTitle,
	FadeAnimation
} from "react-native-popup-dialog";

import Colors from "../../constants/Colors.js";
import {
	setSortField,
	toggleDescList,
	setLoadModeLoad,
	resetPage
} from "../../redux/actions.js";

const SearchSort = props => {
	const [localSortField, setLocalSortField] = useState(props.sortField);
	const [localSortDESC, setLocalSortDESC] = useState(props.sortDESC);
	const options = [
		{ label: "Name", value: "name" },
		{ label: "Prep time", value: "minutes" },
		{ label: "Number of ingredients", value: "n_ingredients" },
		{ label: "Number of steps", value: "n_steps" }
	];

	const handleUseChoices = () => {
		props.dispatch(setSortField(localSortField));
		props.dispatch(setLoadModeLoad());
		props.dispatch(resetPage());
		if (localSortDESC !== props.sortDESC) {
			props.dispatch(toggleDescList());
		}
		props.close();
	};

	return (
		<Dialog
			visible={props.open}
			onTouchOutside={() => {
				props.close();
			}}
			dialogTitle={<DialogTitle title="Sort recipes by" hasTitleBar={false} />}
			dialogAnimation={new FadeAnimation({ animationDuration: 200 })}
			width={0.7}
		>
			<DialogContent>
				<View>
					<View>
						{options.map((option, index) => (
							<ListItem
								key={index}
								onPress={() => setLocalSortField(option.value)}
							>
								<Left>
									<Radio
										selected={localSortField === option.value}
										onPress={() => setLocalSortField(option.value)}
									/>
									<Text style={{ marginLeft: 16 }}>{option.label}</Text>
								</Left>
							</ListItem>
						))}
					</View>
					<View style={{ marginVertical: 20 }}>
						<ListItem
							style={{ borderColor: "white" }}
							onPress={() => setLocalSortDESC(!localSortDESC)}
						>
							<CheckBox
								checked={localSortDESC}
								onPress={() => setLocalSortDESC(!localSortDESC)}
								color={Colors.primary}
							></CheckBox>
							<Body>
								<Text>Descending</Text>
							</Body>
						</ListItem>
					</View>
					<View style={{ marginTop: 16 }}>
						<Button full onPress={() => handleUseChoices()}>
							<Text>use choices</Text>
						</Button>
					</View>
				</View>
			</DialogContent>
		</Dialog>
	);
};

export default SearchSort;
