import React, { useEffect, useState } from "react";
import { StyleSheet } from "react-native";
import { Button, Icon } from "native-base";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";
import { useDeepCompareEffectNoCheck } from "use-deep-compare-effect";

import Colors from "../../constants/Colors";
import { loadRecipe } from "../../utilities/loadUtils";
import { setFavorites } from "../../redux/actions";
import { storeFavorite, removeFavorite } from "../../utilities/asyncStorageUtils";

const FavoriteButton = props => {
	const [localRecipeId, setLocalRecipeId] = useState();
	const [recipe, setRecipe] = useState();
	const [active, setActive] = useState(false);

	useEffect(() => {
		const recipeId = props.navigation.getParam("recipeId");
		if (recipeId) {
			setLocalRecipeId(recipeId);
			loadRecipe(recipeId)
				.then(res => setRecipe(res))
				.catch(error => console.log(error));
		}
	}, []);

	useDeepCompareEffectNoCheck(() => {
		const recipeId = props.navigation.getParam("recipeId");
		if (!localRecipeId) {
			setLocalRecipeId(recipeId);
		}
		setActive(idInFavorites(recipeId));
	}, [props.favorites]);

	function idInFavorites(recipeId) {
		return (
			props.favorites &&
			props.favorites.length &&
			props.favorites.some(favorite => {
				return favorite._id === recipeId;
			})
		);
	}

	function toggleFavorite() {
		const recipeFavoriteFormat = {
			_id: recipe._id,
			name: recipe.name,
			minutes: recipe.minutes,
		};
		if (idInFavorites(localRecipeId)) {
			removeFavorite(recipeFavoriteFormat).then(
				favorites => {
					props.dispatch(setFavorites(favorites));
					setActive(false);
				}
			);
		} else {
			storeFavorite(recipeFavoriteFormat).then(
				favorites => {
					props.dispatch(setFavorites(favorites));
					setActive(true);
				}
			);
		}
	}

	return (
		<Button
			style={styles.favoriteButton}
			transparent
			onPress={() => toggleFavorite()}
		>
			<Icon
				type='MaterialIcons'
				name={active ? "favorite" : "favorite-border"}
				color={Colors.primary}
			/>
		</Button>
	);
};

const mapStateToProps = state => ({
	favorites: state.favorites.favorites,
});

export default connect(mapStateToProps)(withNavigation(FavoriteButton));

const styles = StyleSheet.create({
	favoriteButton: {},
});
