import React, { useEffect, useState } from "react";
import {
	StyleSheet,
	View,
	StatusBar,
	TextInput,
	Platform,
	Text,
} from "react-native";
import { createAppContainer, SafeAreaView } from "react-navigation";
import * as Font from "expo-font";

import AppNavigator from "./navigation/AppNavigator";
import { Ionicons } from "@expo/vector-icons";
import { AppLoading } from "expo";
import { StyleProvider } from "native-base";
import { Provider, connect } from "react-redux";
import store from "./redux/store";
import getTheme from "./native-base-theme/components";
import material from "./native-base-theme/variables/material";
import Colors from "./constants/Colors";
import {
	beginLoadingWordFrequency,
	failureLoadingWordFrequency,
	successLoadingWordFrequency,
} from "./redux/actions";
import { loadWordFrequency } from "./utilities/loadUtils";

TextInput.defaultProps.selectionColor = Colors.primary;

const AppContainer = createAppContainer(AppNavigator);

const App = props => {
	const [isReady, setIsReady] = useState(false);

	useEffect(() => {
		store.dispatch(beginLoadingWordFrequency());
		loadWordFrequency()
			.then(res => store.dispatch(successLoadingWordFrequency(res)))
			.catch(error => store.dispatch(failureLoadingWordFrequency(error)));
	}, []);

	if (Platform.OS === "android") {
		SafeAreaView.setStatusBarHeight(0);
	}

	async function loadFonts() {
		await Font.loadAsync({
			Roboto: require("native-base/Fonts/Roboto.ttf"),
			Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
			Montserrat: require("./assets/fonts/Montserrat-Regular.ttf"),
			Montserrat_Medium: require("./assets/fonts/Montserrat-Medium.ttf"),
			Montserrat_SemiBold: require("./assets/fonts/Montserrat-SemiBold.ttf"),
			Montserrat_Light: require("./assets/fonts/Montserrat-Light.ttf"),
			...Ionicons.font,
		});
		setIsReady(true);
	}

	if (!isReady) {
		return (
			<AppLoading
				startAsync={loadFonts}
				onFinish={() => setIsReady(true)}
				onError={console.warn}
				autoHideSplash={true}
			/>
		);
	}
	return (
		<StyleProvider style={getTheme(material)}>
			<Provider store={store}>
				<View style={{ flex: 1, marginTop: StatusBar.currentHeight }}>
					<AppContainer />
				</View>
			</Provider>
		</StyleProvider>
	);
};

export default App;
